ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jdk8/master:latest

ARG URL=https://dl.bintray.com/sbt/debian
ARG KEY=2EE0EA64E40A89B84B2DF73499E82A75642AC823

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install apt-transport-https gnupg2 && \
    echo "deb ${URL} /" | tee -a /etc/apt/sources.list.d/sbt.list && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv ${KEY} && \
    apt-get update && \
    apt-get -y install sbt && \
    sbt <<EOF \
    exit \
    EOF
