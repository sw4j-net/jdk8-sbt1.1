# Introduction

This project creates a docker image with (open)jdk 8 and sbt 1.1 installed.

The image can be used to run sbt builds.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk8-sbt1.1
